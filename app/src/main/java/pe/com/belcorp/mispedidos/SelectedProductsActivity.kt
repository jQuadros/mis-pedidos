package pe.com.belcorp.mispedidos

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_selected_products.*

class SelectedProductsActivity : AppCompatActivity() {

    private lateinit var linearLayoutManager: LinearLayoutManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_selected_products)

        linearLayoutManager = LinearLayoutManager(this)
        selectedProducts.layoutManager = linearLayoutManager

        val adapter = RecyclerAdapter(getProducts(), this)
        selectedProducts.adapter = adapter

        send.setOnClickListener {
            startActivity(Intent(this, ConfirmationActivity::class.java))
        }

        updateValues()
    }

    fun updateValues() {
        var priceTotal = 0.0
        var storePriceTotal = 0.0

        getProducts().forEach {
            priceTotal += (it.personalPrice * it.quantity)
            storePriceTotal += (it.storePrice * it.quantity)
        }

        total.text = "Total: ${formatValue(priceTotal)}"
        gain.text = "Ganancia: ${formatValue(storePriceTotal - priceTotal)}"
    }

    private fun formatValue(value: Double): String {
        val epsilon = 0.004f
        return if (Math.abs(Math.round(value) - value) < epsilon) {
            var value = "S/${String.format("%10.0f", value).trim()}"
            if (!value.contains(".")) {
                value += ".00"
            }
            value
        } else {
            var value = "S/${String.format("%10.2f", value).trim()}"
            if (!value.contains(".")) {
                value += ".00"
            }
            value
        }
    }
}
