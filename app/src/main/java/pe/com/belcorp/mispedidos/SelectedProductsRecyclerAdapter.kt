package pe.com.belcorp.mispedidos

import android.support.annotation.LayoutRes
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView

class RecyclerAdapter(var selectedProducts: ArrayList<Product>, private val activity: SelectedProductsActivity) : RecyclerView.Adapter<RecyclerAdapter.Holder>()  {

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): Holder {
        val inflatedView = parent.inflate(R.layout.selected_product_item_view, false)
        return Holder(inflatedView)
    }

    override fun getItemCount(): Int {
        return selectedProducts.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val product = selectedProducts[position]
        holder.bind(product) {
            activity.updateValues()
        }
    }

    class Holder(v: View) : RecyclerView.ViewHolder(v), View.OnClickListener {
        override fun onClick(v: View?) {

        }

        private var view: View = v

        init {
            v.setOnClickListener(this)
        }

        fun bind(product: Product, valueUpdated: () -> Any) {
            this.view.findViewById<ImageView>(R.id.productImage).setImageResource(product.image)
            this.view.findViewById<TextView>(R.id.productCode).text = product.code
            this.view.findViewById<TextView>(R.id.productName).text = product.name
            val productQtd = this.view.findViewById<TextView>(R.id.productQtd)
            productQtd.text = product.quantity.toString()
            this.view.findViewById<TextView>(R.id.productPriceUn).text = formatValue(product.storePrice)
            updateValue(product)

            this.view.findViewById<Button>(R.id.productQtdAdd).setOnClickListener {
                if (product.quantity < 99) {
                    product.quantity = product.quantity + 1
                    productQtd.text = product.quantity.toString()
                    updateValue(product)
                    valueUpdated()
                }
            }

            this.view.findViewById<Button>(R.id.productQtdRemove).setOnClickListener {
                if (product.quantity > 0) {
                    product.quantity = product.quantity - 1
                    productQtd.text = product.quantity.toString()
                    updateValue(product)
                    valueUpdated()
                }
            }
        }

        private fun updateValue(product: Product) {
            this.view.findViewById<TextView>(R.id.productPriceTotal).text =
                formatValue(product.quantity * product.storePrice)
        }

        private fun formatValue(value: Double): String {
            val epsilon = 0.004f
            return if (Math.abs(Math.round(value) - value) < epsilon) {
                var value = "S/${String.format("%10.0f", value).trim()}"
                if (!value.contains(".")) {
                    value += ".00"
                }
                value
            } else {
                var value = "S/${String.format("%10.2f", value).trim()}"
                if (!value.contains(".")) {
                    value += ".00"
                }
                value
            }
        }
    }

}

fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
    return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
}