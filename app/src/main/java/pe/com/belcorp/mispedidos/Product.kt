package pe.com.belcorp.mispedidos

data class Product(var code: String, var image: Int, var name: String, var storePrice: Double, var personalPrice: Double, var quantity: Int)

val p1 = Product("Cód. 05849", R.drawable.p1, "MÍA ÉSIKA", 79.80, 49.90, 1)
val p2 = Product("Cód. 05828", R.drawable.p2, "LABIAL HIDRACOLOR XT", 19.40, 10.90, 1)
val p3 = Product("Cód. 29430", R.drawable.p3, "MÁSCARA HD DIVINA", 37.50, 23.90, 1)

fun getProducts(): ArrayList<Product> {

    return arrayListOf(p1, p2, p3)
}