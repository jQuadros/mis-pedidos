package pe.com.belcorp.mispedidos

import android.Manifest
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.drawable.ScaleDrawable
import android.os.Bundle
import android.speech.RecognizerIntent
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.view.MotionEvent
import android.view.View.OnTouchListener
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    private val RECOGNIZE_SPEECH_ACTIVITY = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupPermission()

        cameraBtn.setOnClickListener {
            startActivity(Intent(this, CameraActivity::class.java))
        }

        codeTx.setOnTouchListener(OnTouchListener { _, event ->
            val DRAWABLE_RIGHT = 2

            if (event.action == MotionEvent.ACTION_UP) {
                if (event.rawX >= codeTx.right - codeTx.compoundDrawables[DRAWABLE_RIGHT].bounds.width()) {
                    speak()
                    return@OnTouchListener true
                }
            }
            false
        })

    }

    override fun onActivityResult(
        requestCode: Int, resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode) {
            RECOGNIZE_SPEECH_ACTIVITY ->

                if (resultCode == Activity.RESULT_OK && null != data) {
                    startActivity(Intent(this, SelectedProductsActivity::class.java))
                }
            else -> {
            }
        }
    }

    private fun setupPermission() {
        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }
    }

    private fun makeRequest() {
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), 1)
    }


    fun speak() {

        val intentActionRecognizeSpeech = Intent(
            RecognizerIntent.ACTION_RECOGNIZE_SPEECH
        )

        intentActionRecognizeSpeech.putExtra(
            RecognizerIntent.EXTRA_LANGUAGE_MODEL, "es-MX"
        )

        try {
            startActivityForResult(
                intentActionRecognizeSpeech,
                RECOGNIZE_SPEECH_ACTIVITY
            )
        } catch (a: ActivityNotFoundException) {
            Toast.makeText(
                applicationContext,
                "Tú dispositivo no soporta el reconocimiento por voz",
                Toast.LENGTH_SHORT
            ).show()
        }

    }
}
