package pe.com.belcorp.mispedidos

import android.app.Dialog
import android.content.Intent
import android.os.Bundle
import android.os.Parcel
import android.os.Parcelable
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import android.view.Window
import android.widget.*
import com.androidadvance.topsnackbar.TSnackbar
import com.minibugdev.drawablebadge.BadgePosition
import com.minibugdev.drawablebadge.DrawableBadge
import kotlinx.android.synthetic.main.activity_camera.*

private var totalPrice = 0.0

class CameraActivity() : AppCompatActivity(), Parcelable {

    private var menuCart: Menu? = null

    private var count = 0
    private var productIndex = 0

    constructor(parcel: Parcel) : this() {
        count = parcel.readInt()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)
        setSupportActionBar(camera_toolbar)
        setToolbarTitle("Total: S/0.0")

        addProductToList()
    }

    private fun setToolbarTitle(title: String) {
        supportActionBar?.title = title
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuCart = menu
        menuInflater.inflate(R.menu.menu_camera, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {

        if (item != null) {
            if (item.itemId == R.id.menu_done) {
                startActivity(Intent(this, SelectedProductsActivity::class.java))
                return true
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun addProductToList() {
        takePic.setOnClickListener {
            val product = getProducts()[productIndex]
            var productQtdInt = 1

            val dialog = Dialog(this)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.setCancelable(false)
            dialog.setContentView(R.layout.confirmation_dialog)

            val productQtdRemove = dialog.findViewById<Button>(R.id.productQtdRemove)
            val productQtdAdd = dialog.findViewById<Button>(R.id.productQtdAdd)
            val productQtd = dialog.findViewById<EditText>(R.id.productQtd)
            val confirmationDoneBtn = dialog.findViewById<Button>(R.id.confirmationDoneBtn)

            val confirmationProductName = dialog.findViewById<TextView>(R.id.confirmationProductName)
            val productConfirmationPrice = dialog.findViewById<TextView>(R.id.productConfirmationPrice)
            val confirmationPic = dialog.findViewById<ImageView>(R.id.confirmationPic)
            val close = dialog.findViewById<Button>(R.id.close)

            productQtd.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                    if (productQtd.text.toString().isNotEmpty()) {
                        productQtdInt = Integer.parseInt(productQtd.text.toString())

                        if (Integer.parseInt(productQtd.text.toString()) > 99) {
                            productQtd.setText("99")
                        }
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            })

            productQtdRemove.setOnClickListener {
                if (productQtdInt > 1) {
                    productQtd.setText((--productQtdInt).toString())
                }

                productQtdRemove.isEnabled = productQtdInt != 1
            }

            productQtdAdd.setOnClickListener {
                if (productQtdInt < 99) {
                    productQtd.setText((++productQtdInt).toString())
                }

                productQtdRemove.isEnabled = productQtdInt != 1
            }

            confirmationDoneBtn.setOnClickListener {

                if (productIndex < 2) {
                    ++productIndex
                }

                product.quantity = productQtdInt

                val snack = TSnackbar.make(camera_toolbar, "Producto agregado en la lista", TSnackbar.LENGTH_SHORT)
                val snackView = snack.view
                snackView.findViewById<TextView>(com.androidadvance.topsnackbar.R.id.snackbar_text).setTextColor(ContextCompat.getColor(this, android.R.color.white))
                val params = LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
                )
                params.setMargins(0, 0, 0, 0)
                snackView.layoutParams = params

                snack.show()

                dialog.dismiss()

                DrawableBadge.Builder(applicationContext)
                    .drawableResId(R.drawable.cart)
                    .badgeColor(R.color.badge_color)
                    .badgeSize(R.dimen.default_badge_size)
                    .badgePosition(BadgePosition.TOP_RIGHT)
                    .textColor(R.color.default_badge_text_color)
                    .showBorder(true)
                    .badgeBorderColor(R.color.badge_color)
                    .badgeBorderSize(R.dimen.default_badge_border_size)
                    .maximumCounter(99)
                    .build()
                    .get(++count)
                    .let { drawable ->
                        menuCart?.getItem(0)?.setIcon(drawable)
                    }

                setToolbarTitle(getTotalPrice(productQtdInt, product.storePrice))
            }

            close.setOnClickListener {
                dialog.dismiss()
            }

            confirmationProductName.text = product.name
            productConfirmationPrice.text = "S/ ${product.storePrice}"
            confirmationPic.setImageResource(product.image)

            dialog.show()
        }
    }

    private fun getTotalPrice(productQtdInt: Int, storePrice: Double): String {
        val epsilon = 0.004f
        return if (Math.abs(Math.round(storePrice) - storePrice) < epsilon) {
            totalPrice += (productQtdInt * storePrice)
            "Total: S/${String.format("%10.0f", totalPrice).trim()}"
        } else {
            totalPrice += (productQtdInt * storePrice)
            "Total: S/${String.format("%10.2f", totalPrice).trim()}"
        }
    }

    override fun onResume() {
        super.onResume()
        cameraView.start()
    }

    override fun onPause() {
        cameraView.stop()
        super.onPause()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(count)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<CameraActivity> {
        override fun createFromParcel(parcel: Parcel): CameraActivity {
            return CameraActivity(parcel)
        }

        override fun newArray(size: Int): Array<CameraActivity?> {
            return arrayOfNulls(size)
        }
    }
}
